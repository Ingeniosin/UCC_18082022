package me.juan.learning;

import me.juan.learning.entity.persons.Athlete;
import me.juan.learning.entity.persons.Firefighter;
import me.juan.learning.entity.persons.Person;
import me.juan.learning.entity.persons.Teacher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();

        Teacher andres = new Teacher("Andres", 30, true, 1.80, new Date());
        Athlete juan = new Athlete("Juan", 25, true, 1.80, new Date());
        Firefighter jose = new Firefighter("Jose", 35, true, 1.80, new Date());

        persons.add(andres);
        persons.add(juan);
        persons.add(jose);

        persons.forEach(System.out::println);

    }

}
