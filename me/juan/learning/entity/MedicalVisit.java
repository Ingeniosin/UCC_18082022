package me.juan.learning.entity;

import lombok.Data;

import java.util.Date;

@Data
public class MedicalVisit {

    public Date date;
    public String diagnosis;
    public String symptoms;

}
