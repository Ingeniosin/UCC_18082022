package me.juan.learning.entity.persons;

import java.util.Date;

public class Firefighter extends Person {

    public Firefighter(String name, int age, boolean isActive, double height, Date birthDate) {
        super(name, age, isActive, height, birthDate);
    }

    @Override
    public void work() {
        super.work();
    }

}
