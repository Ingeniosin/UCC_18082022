package me.juan.learning.entity.persons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
public class Person {

    public String name;
    public int age;
    public boolean isActive;
    public double height;
    public Date birthDate;

    public void work() {
        System.out.println("I'm working");
    }

}
